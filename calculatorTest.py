from calculator import Calculator

class CalculatorTest:

    def __init__(self):
        self.calculator = Calculator()
    
    def testAdd(self):
        check = True
        if self.calculator.add(1,1) != 2 :
            print("addition error! 1+1=" + self.calculator.add(1,1) + "?" )
            check = False
        if self.calculator.add(1,2) != 3 :
            print("addition error! 1+2=" + self.calculator.add(1,2) + "?" )
            check = False
        if self.calculator.add(1,-2) != -1 :
            print("addition error! 1+(-2)=" + self.calculator.add(1,-2) + "?" )
            check = False
        return check

    def testSub(self):
        check = True
        if self.calculator.sub(1,1) != 0 :
            print("substraction error! 1-1=" + self.calculator.sub(1,1) + "?" )
            check = False
        if self.calculator.sub(1,2) != -1 :
            print("substraction error! 1-2=" + self.calculator.sub(1,2) + "?" )
            check = False
        if self.calculator.sub(1,-2) != 3 :
            print("substraction error! 1-(-2)=" + self.calculator.sub(1,-2) + "?" )
            check = False
        return check
    
    def testMul(self):
        check = True
        if self.calculator.mul(1,1) != 1 :
            print("multiplication error! 1*1=" + self.calculator.mul(1,1) + "?" )
            check = False
        if self.calculator.mul(1,2) != 2 :
            print("multiplication error! 1*2=" + self.calculator.mul(1,2) + "?" )
            check = False
        if self.calculator.mul(1,-2) != -2 :
            print("multiplication error! 1*(-2)=" + self.calculator.mul(1,-2) + "?" )
            check = False
        return check
    
    def testDiv(self):
        check = True
        if self.calculator.div(1,1) != 1 :
            print("division error! 1/1=" + self.calculator.div(1,1) + "?" )
            check = False
        if self.calculator.div(1,2) != 0.5 :
            print("division error! 1/2=" + self.calculator.div(1,2) + "?" )
            check = False
        if self.calculator.div(1,-2) != -0.5 :
            print("division error! 1/(-2)=" + self.calculator.div(1,-2) + "?" )
            check = False
        return check

    def run(self):
        addCheck = self.testAdd()
        subCheck = self.testSub()
        mulCheck = self.testMul()
        divCheck = self.testDiv()
        if addCheck :
            print("addition OK!")
        if subCheck :
            print("substraction OK!")
        if mulCheck :
            print("multiplication OK!")
        if divCheck :
            print("division OK!")